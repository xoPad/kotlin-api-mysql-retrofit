package com.example.kotlinretrofitapimysql.models

data class DefaultResponse (
    val error:Boolean,
    val message:String
)